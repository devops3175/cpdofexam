package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {
    @Test
	public void testbGreaterThanACase1() throws Exception {
		int k = new MinMax().bGreaterThanA(1, 4);{
			assertEquals("B greater than A", 4, k);

		}	

	}
	@Test
        public void testbGreaterThanACase2() throws Exception {
                int k = new MinMax().bGreaterThanA(1, 0);{
                        assertEquals("B less than A", 1, k);

                }

        }

	@Test
        public void testStringNotNULL() throws Exception {
                String k = new MinMax().bar("String");{
                        assertEquals("String not empty", "String", k);

                }
	}
        @Test
        public void testStringNULL() throws Exception {
                String k = new MinMax().bar("");{
                        assertEquals("String empty", "", k);

                }


        }


}
